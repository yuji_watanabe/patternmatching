﻿#include <iostream>

bool match(char *pattern, char *text) {
    switch (*pattern) {
      case '\0':
        return !*text;
      case '*' :
        return match(pattern + 1, text) || (*text && match(pattern, text + 1));
      case '?' :
        return *text && match(pattern + 1, text + 1);
      default  :
        return (*text == *pattern) && match(pattern + 1, text + 1);
    }
}

int main(int argc, char **argv)
{
    if (argc >= 3) {
        if (match(argv[1], argv[2])) {
            std::cout << "Match!" << std::endl;
        } else {
            std::cout << "Mismatch!" << std::endl;
        }
    } else {
        std::cout << "Error: Few arguments";
    }
    return 0;
}
