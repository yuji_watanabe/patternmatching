Backtrack algorithm
====

for example...

    match("*c.?xt", "abc.txt")

~~~~
pattern * → 全てに一致するので、次のパターンに進める
pattern c → c は default なので、c と a を比較する。不一致なので false で戻る
pattern * → match(pattern + 1, text)が false だったので、|| (*text && match(pattern, text + 1))に進む
pattern * → 全てに一致するので、次のパターンに進める
pattern c → c は default なので、c と b を比較する。不一致なので false で戻る
pattern * → match(pattern + 1, text)が false だったので、|| (*text && match(pattern, text + 1))に進む
pattern * → 全てに一致するので、次のパターンに進める
pattern c → c は default なので、c と c を比較する。一致だったので、&& match(pattern + 1, text + 1)に進む
pattern . → . は default なので、. と . を比較する。一致だったので、&& match(pattern + 1, text + 1)に進む
pattarn ? → ? は text が null でなければ一致なので、&& match(pattern + 1, text + 1)に進む
pattarn x → x は default なので、x と x を比較する。一致だったので、&& match(pattern + 1, text + 1)に進む
pattern t → t は default なので、t と t を比較する。一致だったので、&& match(pattern + 1, text + 1)に進む
pattern null → は、\0 なので、text が null かを確認する。null だったので、true を返す。
~~~~
